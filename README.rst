..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST read me file

 Author         : Dirk Ullrich

 Date           : 2020-12-21

 Description    : Read me file for package ``duargparse``.
 =============================================================================


================================================
``duargparse``: extensions for py:mod:`argparse`
================================================

This package provides extensions for the :py:mod:`argparse` module from the
Python Standard Library.

At the moment if defines a subclass :py:class:`DUArgumentParser` for
:py:class:`ArgumentParser` from :py:mod:`argparse` such that you can customize
text snippets that are part of some standard help messages.  The defaults for
these text snippets are chosen to match my preferences.

The class :py:class:`DUArgumentParser` from :py:mod:`duargparse` can be used
exactly like :py:class:`ArgumentParser` from standard :py:mod:`argparse`.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
