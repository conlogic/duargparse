..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-12-27

 Description    : Change log for the package.
 =============================================================================


=========
Changelog
=========


Version 1.0.3 (2021-12-27)
==========================

- Add support for Python 3.10.


Version 1.0.2 (2021-07-05)
==========================

- Migrate package tools to ``f"..."``-strings and :py:mod:`pathlib`.

- Migrate package itself to ``f"..."``-strings.

- Fix development status (via ``classifiers`` for ``setup``).

- Drop support for Python 3.5, since ``f"..."``-strings are used (via
  ``classifiers`` for ``setup``).


Version 1.0.1 (2021-03-19)
==========================

- Customize Sphinx HTML theme.

- Proper Sphinx-compatible formatting of the ``CHANGELOG`` file.


Version 1.0 (2021-02-27)
========================

- Fix the usage documentation.


Version 0.4.5 (2021-02-13)
==========================

- Fix helper function in configuration file for the Sphinx-based
  documentation.


Version 0.4.4 (2021-01-06)
==========================

- Use consistent format for package's lifetime.
- Update copyright for 2021.


Version 0.4.3 (2020-21-21)
==========================

- Fix formatting for the read me file and some Python modules.


Version 0.4.2 (2020-21-21)
==========================

- Fix a typo in file header titles.


Version 0.4.1 (2020-12-06)
==========================

- Reverse order in the ``CHANGELOG.rst`` file.

- Add copyright + license info to :py:mod:`duargparse.py` to allow standalone
  use of this module.

- Declare support for Python 3.9, too.


Version 0.4 (2020-10-01)
========================

- Make the customized parts of the argument parser message parts
  customizable by additional keyword arguments.  The former, hard-coded,
  customizations of those message parts now are the default values of these
  additional keyword arguments.


Version 0.3.2 (2020-09-30)
==========================

- Move as much :py:func:`setup` arguments into ``setup.cfg`` as possible.

- Add a change log file.

- Add Sphinx-based documentation of the package.


Version 0.3.1 (2020-08-26)
==========================

- Cosmetics -- move the version attribute into the main module.


Version 0.3 (2020-08-25)
========================

- Make documentation compatible to Sphinx.

- Re-implement former functionality using an own help message formatter class.

- Using the new help message formatter, to customize the default formatting
  for meta variables in help messages, and provide a small helper function for
  this.


Version 0.2 (2020-07-07)
========================

- Add a customized help message for the standard ``version`` action.


Version 0.1.2 (2020-01-26)
==========================

- Adapt to use the standard Python mode of Emacs.

- Update supported Python versions.

- Fix small bugs reported by ``flake8``.


Version 0.1.1.1 (2018-11-24)
============================

- Declare support for Python 3.7, too.


Version 0.1.1 (2018-08-09)
==========================

- Use my then current code and doc conventions.


Version 0.1 (2018-05-17)
========================

- Initial version: Add a customized help message for the standard ``help``
  action.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
