# ============================================================================
# Title          : Extensions for :py:mod:`argparse`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-12-27
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2018-2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Single module for package ``duargparse``.

Own extensions for module :py:mod:`argparse` from Standard Library.
'''


import argparse


# Parameters
# ==========

# Version of this package.
version = '1.0.3'

# Default prefix for usage within the main help message.
USAGE_PREFIX = ' \nUsage: '

# Default title for position arguments in the main help message.
POS_TITLE = 'Arguments'

# Default title for option arguments in the main help message.
OPT_TITLE = 'Options'

# Default help message for the standard version action.
VERSION_HELP = "Show program's version and exit."

# Default help message for the standard help action.
HELP_HELP = 'Show this help message and exit.'


# Functions and classes
# =====================


def metavar(param):
    '''
    Format parameter `param` as meta variable for itself.

    :return: the meta variable for `param`.
    '''
    # We only add ``"<"`` and ``">"`` if necessary.
    beg = '' if param.startswith('<') else '<'
    end = '' if param.endswith('>') else '>'
    meta = beg + param + end

    return meta


class DUHelpFormatter(argparse.HelpFormatter):
    '''
    Own help formatter to customize help messages.  It differs from the
    standard help formatter by the following additional keyword arguments:

    - `usage_prefix`: usage prefix in the main help message.
    - `metavar_fct`: function to be called for deriving meta variables from
      argument destination attributes.
    '''
    def __init__(self, *posargs,
                 usage_prefix=USAGE_PREFIX,
                 metavar_fct=metavar,
                 **kwargs):
        '''
        Initialize the help formatter where the additional keyword arguments
        are described in the :py:class:`DUHelpFormatter` documentation.
        '''
        self.usage_prefix = usage_prefix
        self.metavar_fct = metavar_fct
        super().__init__(*posargs, **kwargs)


    def _get_default_metavar_for_optional(self, action):
        dest = action.dest
        meta = self.metavar_fct(dest)

        return meta


    def _get_default_metavar_for_positional(self, action):
        dest = action.dest
        meta = self.metavar_fct(dest)

        return meta


    def _format_usage(self, usage, actions, groups, prefix):
        '''
        Own version of :py:meth:`_format_usage` to use a custom default
        prefix.
        '''
        if prefix is None:
            prefix = self.usage_prefix
        return super(DUHelpFormatter, self)._format_usage(
            usage, actions, groups, prefix)


class DUArgumentParser(argparse.ArgumentParser):
    '''
    Simple extension of :py:class:`ArgumentParser` for :py:mod:`argparse`:
    It differs from the standard class by changing some parts of its
    messages.  This is controlled by the following parameters of the
    :py:meth:`__init__` method:

    - `usage_prefix`: usage prefix in the main help message.
    - `pos_title`: title for position arguments in the main help message.
    - `opt_title`: title for option arguments in the main help message.
    - `version_help`: help string for the standard version action.
    - `help_help`: help string for the standard help action.
    - `metavar_fct`: function to be called for deriving meta variables from
      argument destination attributes.
    '''
    def __init__(self, *posargs,
                 usage_prefix=USAGE_PREFIX,
                 pos_title=POS_TITLE,
                 opt_title=OPT_TITLE,
                 help_help=HELP_HELP,
                 version_help=VERSION_HELP,
                 metavar_fct=metavar,
                 **kwargs):
        '''
        Initialize an argument parser.  For a description of the additional
        keyword arguments see the documentation of
        :py:class:`DUArgumentParser`.
        '''
        # Use own help formatter class by default.
        if 'formatter_class' not in kwargs:

            # - Create a closure class with the customized values set.
            class _MyHelpFormatter(DUHelpFormatter):
                def __init__(self, *posargs, **kwargs):
                    super().__init__(
                        *posargs, usage_prefix=usage_prefix,
                        metavar_fct=metavar_fct, **kwargs)

            # - Use our closure class.
            kwargs['formatter_class'] = _MyHelpFormatter

        super().__init__(*posargs, **kwargs)

        # Fix titles for argument sections.
        self._positionals.title = pos_title
        self._optionals.title = opt_title

        # Fix message format for help option.
        for act in self._optionals._actions:
            if act.__class__ == argparse._HelpAction:
                act.help = help_help

        # Register own customized version action as ``version``.

        # - Create, again, an appropriate closure class.
        class _MyVersionAction(argparse._VersionAction):
            def __init__(self, *posargs, **kwargs):
                super().__init__(*posargs, **kwargs)
                self.help = version_help

        # - Register our closure class.
        self.register('action', 'version', _MyVersionAction)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
