..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2020-12-21

 Description    : Package overview.
 =============================================================================


===================================
General information for the package
===================================

.. toctree::
   :maxdepth: 1

   readme

   license

   changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
