..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2020-12-21

 Description    : Read me for the package.
 =============================================================================


.. include:: ../README.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
