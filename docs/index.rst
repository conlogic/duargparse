..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2020-12-21

 Description    : Master file for the documentation.
 =============================================================================


================================
Documentation for ``duargparse``
================================

.. toctree::
   :maxdepth: 2

   overview
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
