..
 =============================================================================
 Title          : Extensions for :py:mod:`argparse`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2020-12-27

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================

This package consists of exactly one module -- namely :py:mod:`duargparse`
that extends module :py:mod:`argparse` from the Python Standard Library.

First of all, it provides class :py:class:`DUArgumentParser` as a drop-in
replacement of the standard :py:class:`ArgumentParser`.  Its
:py:meth:`__init__` method provides some additional keyword arguments to
customize parts of the messages emitted by the argument parser.

.. autoclass:: duargparse.DUArgumentParser

The default values for these additional keyword arguments represent my own
preferences for the respective message parts.

Beside this, a small helper function is provided to format meta variables
within help messages:

.. autofunction:: duargparse.metavar

This function :py:func:`metavar` is also used to derive meta variables from
parameter destinations.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
