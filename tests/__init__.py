# ============================================================================
# Title          : Extensions for :py:mod:`argparse`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2020-12-21
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``duargparse``.
'''


# This turns the ``tests`` directory into a Python package.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
