# ============================================================================
# Title          : Extensions for :py:mod:`argparse`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-05
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`duargparse` of package ``duargparse``.
'''


import unittest as ut

from duargparse import (
    metavar, DUArgumentParser,
    USAGE_PREFIX, OPT_TITLE, POS_TITLE, HELP_HELP, VERSION_HELP)


class metavar_TC(ut.TestCase):
    '''
    Tests for the :py:func:`metavar` function.
    '''
    def test_no_delim(self):
        self.assertSequenceEqual(metavar('foo'), '<foo>')


    def test_l_delim(self):
        self.assertSequenceEqual(metavar('<foo'), '<foo>')


    def test_r_delim(self):
        self.assertSequenceEqual(metavar('foo>'), '<foo>')


    def test_lr_delim(self):
        self.assertSequenceEqual(metavar('<foo>'), '<foo>')


def alt_metavar(dst):
    '''
    An alternative function to derive meat variables.
    '''
    return ('|' + dst + '|')


class DUArgumentParser_base_TC(ut.TestCase):
    '''
    Base class for tests of the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        # Argument parsers with default customizations.
        self.parser_noargs = DUArgumentParser()
        self.parser_args = DUArgumentParser()
        self.parser_args.add_argument('foo', help='A dummy argument.')
        self.parser_args.add_argument(
            '--version', action='version', version="%(prog) 13")
        self.parser_args.add_argument(
            '--foo-bar', action='store', help='foobar option')
        self.parser_args.add_argument(
            '--baz', metavar='[baz]', action='store', help='baz option')

        # Argument parsers with German customizations.
        # - German customizations.
        self.usage_prefix_de = ' \nVerwendung: '
        self.pos_title_de = 'Argumente'
        self.opt_title_de = 'Optionen'
        self.version_help_de = "Ausgabe der Programmversion und Ende."
        self.help_help_de = "Ausgabe dieses Hilfetextes und Ende."
        # - Real parsers.
        self.parser_noargs_de = DUArgumentParser(
            usage_prefix=self.usage_prefix_de,
            pos_title=self.pos_title_de, opt_title=self.opt_title_de,
            version_help=self.version_help_de, help_help=self.help_help_de)
        self.parser_args_de = DUArgumentParser(
            usage_prefix=self.usage_prefix_de,
            pos_title=self.pos_title_de, opt_title=self.opt_title_de,
            version_help=self.version_help_de, help_help=self.help_help_de,
            metavar_fct=alt_metavar)
        self.parser_args_de.add_argument('foo', help='Ein Dummy-Argument.')
        self.parser_args_de.add_argument(
            '--version', action='version', version="%(prog) 13")
        self.parser_args_de.add_argument(
            '--foo-bar', action='store', help='Option foobar')
        self.parser_args_de.add_argument(
            '--baz', metavar='[baz]', action='store', help='Option baz')


class DUArgumentParser_usage_header_TC(DUArgumentParser_base_TC):
    '''
    Test the usage header for the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        super().setUp()
        self.usage_header = USAGE_PREFIX
        self.usage_header_de = self.usage_prefix_de


    def test_format_usage(self):
        '''
        Test for function :py:func:`format_usage`.
        '''
        usage_msg = self.parser_noargs.format_usage()
        self.assertIn(self.usage_header, usage_msg)


    def test_format_usage_de(self):
        '''
        Test for function :py:func:`format_usage`.
        '''
        usage_msg = self.parser_noargs_de.format_usage()
        self.assertIn(self.usage_header_de, usage_msg)


    def test_format_help_noargs(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(self.usage_header, self.parser_noargs.format_help())


    def test_format_help_noargs_de(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(
            self.usage_header_de, self.parser_noargs_de.format_help())


    def test_format_help_args(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(self.usage_header, self.parser_args.format_help())


    def test_format_help_args_de(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(
            self.usage_header_de, self.parser_args_de.format_help())


class DUArgumentParser_opts_header_TC(DUArgumentParser_base_TC):
    '''
    Test the options header for the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        super().setUp()
        self.opts_header = f"\n{OPT_TITLE}:\n"
        self.opts_header_de = f"\n{self.opt_title_de}:\n"


    def test_format_help_noargs(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(self.opts_header, self.parser_noargs.format_help())


    def test_format_help_noargs_de(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(
            self.opts_header_de, self.parser_noargs_de.format_help())


    def test_format_help_args(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(self.opts_header, self.parser_args.format_help())


    def test_format_help_args_de(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(
            self.opts_header_de, self.parser_args_de.format_help())


class DUArgumentParser_args_header_TC(DUArgumentParser_base_TC):
    '''
    Test the arguments header for the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        super().setUp()
        self.args_header = f"\n{POS_TITLE}:\n"
        self.args_header_de = f"\n{self.pos_title_de}:\n"


    def test_format_help_noargs(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertNotIn(self.args_header, self.parser_noargs.format_help())


    def test_format_help_noargs_de(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertNotIn(
            self.args_header_de, self.parser_noargs_de.format_help())


    def test_format_help_args(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(self.args_header, self.parser_args.format_help())


    def test_format_help_args_de(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(
            self.args_header_de, self.parser_args_de.format_help())


class DUArgumentParser_help_TC(DUArgumentParser_base_TC):
    '''
    Test the help message for the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        super().setUp()
        self.help_message = HELP_HELP + '\n'
        self.help_message_de = self.help_help_de + '\n'


    def test_format_help_noargs(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(self.help_message, self.parser_noargs.format_help())


    def test_format_help_noargs_de(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertIn(
            self.help_message_de, self.parser_noargs_de.format_help())


    def test_format_help_args(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(self.help_message, self.parser_args.format_help())


    def test_format_help_args_de(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(
            self.help_message_de, self.parser_args_de.format_help())


class DUArgumentParser_version_TC(DUArgumentParser_base_TC):
    '''
    Test the version message for the :py:class:`DUArgumentParser` class.
    '''
    def setUp(self):
        super().setUp()
        self.vers_message = VERSION_HELP + '\n'
        self.vers_message_de = self.version_help_de + '\n'


    def test_format_help_noargs(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertNotIn(self.vers_message, self.parser_noargs.format_help())


    def test_format_help_noargs_de(self):
        '''
        Test for function :py:func:`format_help` without arguments.
        '''
        self.assertNotIn(
            self.vers_message_de, self.parser_noargs_de.format_help())


    def test_format_help_args(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(self.vers_message, self.parser_args.format_help())


    def test_format_help_args_de(self):
        '''
        Test for function :py:func:`format_help` with arguments.
        '''
        self.assertIn(
            self.vers_message_de, self.parser_args_de.format_help())


class DUArgumentParser_metavar_TC(DUArgumentParser_base_TC):
    '''
    Test the meta variable formatting for the :py:class:`DUArgumentParser`
    class.
    '''


    def test_default_metavar(self):
        self.assertIn('<foo_bar>', self.parser_args.format_help())


    def test_given_metavar(self):
        self.assertNotIn('<baz>', self.parser_args.format_help())
        self.assertIn('[baz]', self.parser_args.format_help())


    def test_alt_default_metavar(self):
        self.assertIn('|foo_bar|', self.parser_args_de.format_help())


    def test_alt_given_metavar(self):
        self.assertNotIn('|baz|', self.parser_args_de.format_help())
        self.assertIn('[baz]', self.parser_args_de.format_help())


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
